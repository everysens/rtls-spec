# API convoi

## Déclaration du plan de transport

Voir la documentation sur [GTFS](https://gitlab.com/everysens/rtls-spec/operail/gtfs).

## Ajout de wagons dans un convoi

L'ajout de wagons se fait avec un POST sur cette url: `https://track.everysens.com/rtls-things-app/convoys/integration/v1/rail-pl`

Un HTTP POST contenant le JSON suivant:

```
{
  "sillon": "nomDuSillon",
  "departureDate": "2017-06-27T09:16:00Z",
  "arrivalDate": "2017-06-27T22:27:00Z",
  "departureZone": "Paris",
  "arrivalZone": "Montpellier",
  "name": "nomDuConvoi",
  "participants" : ["p1", "p2", "p3"]
}
```
permet d'affecter au sillon `nomDuSillon` (équivalent au champ `route_short_name` du fichier GTFS `routes.txt`), les participants `p1`, `p2` et `p3`.

Les champs `departureDate` et `arrivalDate` sont des champs date au format ISO 8601. **⚠ Les dates sont fournies en timezone UTC ⚠**.

Les champs `departureZone` et `arrivalZone` sont mandatory et sont censés être respectivement le `stop_name` du premier et dernier Stop définis dans le plan de transport GTFS. Si ces valeurs différent du plan de transport, l'application enregistrera ce sillon comme sillon temporaire.

Le champ `name` permet de définir le nom du convoi associé. Il est unique: si on essaye d'associer le même `name` a des convois faisant un trajet différent, le premier sera accepté, tout les convois suivants seront rejetés.

Cette requête est idem-potente: on peut la rejouer plusieurs fois d'affilée sans problème.

> ⚠ Attention: ⚠

Tout les champs doivent matcher exactement pour être pris en compte et doivent être définis dans le plan de transport. Par exemple, si le `stop_name` du fichier GTFS est `Gare de Paris-Nord` et que le JSON contient `"departureZone": "Gare de Paris Nord"`, l'application retournera une erreur.

Le fichier GTFS fait source de vérité et on considère que des garde-fous ont été appliqué en amont. Ainsi, par exemple, si le fichier GTFS déclare que la gare de Paris est sur la timezone `Asia/Tokyo`, l'application utilisera cette timezone sans avertissements.

## Suppression/Modifications des participants dans un convoi

Pour modifier ou supprimer des participants d'un convoi, il suffit de renvoyer le JSON précédemment avec la liste de participants actualisée.

Par exemple, si on veut retirer le wagon `p1` et ajouter `p5` et `p6`:

```
{
  "sillon": "nomDuSillon",
  "departureDate": "2017-06-27T09:16:00Z",
  "arrivalDate": "2017-06-27T22:27:00Z",
  "departureZone": "Paris",
  "arrivalZone": "Montpellier",
  "name": "nomDuConvoi",
  "participants" : ["p2", "p3", "p5", "p6"]
}
```