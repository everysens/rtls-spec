# Plan de transport

## Déclaration du plan de transport

Le plan de transport doit être fourni au format [GTFS](https://developers.google.com/transit/gtfs).

General Transit Feed Specification (GTFS, traduction littérale : spécification générale pour les flux relatifs aux transports en commun) est un format informatique standardisé pour communiquer des horaires de transports en commun et les informations géographiques associées (topographie d'un réseau : emplacement des arrêts, tracé des lignes).
 
Tout les champs `required` sont effectivement `required`: l'application refusera tout champs omis.

Les champs optionnels pouvant être inféré, comme le `stop_timezone` du fichier `stops.txt` ou `shape_dist_traveled` du fichier `stop_times.txt`, seront réécrits si une entrée (ou plus) n'est pas renseigné. On ne garde les valeurs fournies que si *toutes* sont remplies.

## Import du plan de transport

Pour l'instant, l'import se fait manuellement par Everysens. Une API permettant d'automatiser cette tâche est à prévoir.

> ⚠ Attention: ⚠

Le fichier GTFS fait source de vérité et on considère que des garde-fous ont été appliqué en amont par le client. Ainsi, par exemple, si le fichier GTFS déclare que la gare de Paris est sur la timezone `Asia/Tokyo`, l'application utilisera cette timezone sans avertissements.


## Outils notables

[AddTransit](https://addtransit.com/) est un bon GUI pour remplir les fichiers GTFS.

[transitfeed](https://github.com/google/transitfeed) sont des outils fournis par Google pour visualiser et valider les fichiers GTFS.