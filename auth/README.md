# Everysens : API d'authentification

## Demande d'un Token d'accès par une application
Une application externe enregistrée peut demander un Bearer token afin de pouvoir utiliser les API d'Everysens. 

> L'application doit déjà avoir été enregistrée intialement sur le serveur d'authentification d'Everysens.   
> Si ce n'est pas le cas, rapprochez-vous de votre contact technique chez Everysens.

Effectuez une requête HTTP POST sur le serveur d'authentification OpenIdConnect d'Everysens en remplaçant les valeurs suivantes :
- `<tenant>` est le nom de code du compte client chez everysens
- `<client_id>` est le nom de code de l'application cliente
- `<client_secret>` est le mot de passe de l'application cliente

Ci-dessous, plusieurs examples dans différents langages :

HTTP:
```
POST /auth/realms/everysens/protocol/openid-connect/token HTTP/1.1
Host: track.everysens.com
Content-Type: application/x-www-form-urlencoded

grant_type=client_credentials&client_id=<client_id>&client_secret=<client_secret>
```

cURL:
```
curl -X POST \
  https://track.everysens.com/auth/realms/everysens/protocol/openid-connect/token \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'grant_type=client_credentials&client_id=<client_id>&client_secret=<client_secret>'
```


NodeJS:
```
var request = require("request");

var options = { method: 'POST',
  url: 'https://track.everysens.com/auth/realms/everysens/protocol/openid-connect/token',
  headers: 
   { 'Content-Type': 'application/x-www-form-urlencoded' },
  form: 
   { grant_type: 'client_credentials',
     client_id: '<client_id>',
     client_secret: '<client_secret>' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});

```

Java:
```
OkHttpClient client = new OkHttpClient();

MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
RequestBody body = RequestBody.create(mediaType, "grant_type=client_credentials&client_id=<client_id>&client_secret=<client_secret>");
Request request = new Request.Builder()
  .url("https://track.everysens.com/auth/realms/everysens/protocol/openid-connect/token")
  .post(body)
  .addHeader("Content-Type", "application/x-www-form-urlencoded")
  .build();

Response response = client.newCall(request).execute();
```

PHP:
```
<?php

$request = new HttpRequest();
$request->setUrl('https://track.everysens.com/auth/realms/everysens/protocol/openid-connect/token');
$request->setMethod(HTTP_METH_POST);

$request->setHeaders(array(
  'Content-Type' => 'application/x-www-form-urlencoded'
));

$request->setContentType('application/x-www-form-urlencoded');
$request->setPostFields(array(
  'grant_type' => 'client_credentials',
  'client_id' => '<client_id>',
  'client_secret' => '<client_secret>'
));

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```

Python 3:
```
import http.client

conn = http.client.HTTPConnection("track.everysens.com")

payload = "grant_type=client_credentials&client_id=<client_id>&client_secret=<client_secret>"

headers = {
    'Content-Type': "application/x-www-form-urlencoded"
}

conn.request("POST", "auth/realms/everysens/protocol/openid-connect/token", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

C#:
```
var client = new RestClient("https://track.everysens.com/auth/realms/everysens/protocol/openid-connect/token");
var request = new RestRequest(Method.POST);
request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
request.AddParameter("undefined", "grant_type=client_credentials&client_id=<client_id>p&client_secret=<client_secret>", ParameterType.RequestBody);
IRestResponse response = client.Execute(request);
```


La réponse est au format JSON selon la structure suivante : 
```
{
  "access_token": "eyJh[...]WTtA",
  "expires_in": 7776000,
  "refresh_expires_in": 604800,
  "refresh_token": "eyJh[...]AHQg",
  "token_type": "bearer",
  "id_token": "eyJh[...]SKBsQ",
  "not-before-policy": 0,
  "session_state": "d1d7[...]babb"
}

```
Le token d'authentification est `access_token`.  
`expires_in` indique la durée en secondes au delà de laquelle ce token devient invalide.  
Le token peut aussi devenir prématurément invalide en cas d'expiration manuelle par everysens.  
Si le token devient invalide, il est possible d'en demander un autre en répétant la requête d'authentification ci-dessus.   

## Exemple d'utilisation

On récupère le token via la commande `jq` en bash:
```
export ACCESS_TOKEN=$(curl -X POST https://track.everysens.com/auth/realms/everysens/protocol/openid-connect/token  -H "Content-Type: application/x-www-form-urlencoded" -d "grant_type=client_credentials" -d 'client_id=<client_id>' -d 'client_secret=<client_secret>' | jq -r '.access_token')
```

On effectue une requête sur une des API Everysens en tant que votre tenant :
```
curl https://track.everysens.com/rtls-things-app/api-info  
```

La réponse attendue sans token ou avec un token invalide est une erreur 401 ou 403:
```
{
  "httpStatus": 401,
  "httpReason": "Unauthorized",
  "apiErrorCode": "everysens.api.authorization.unauthorized",
  "userMessage": "You don't have access to this resource",
  "developerMessage": "Unauthorized"
}
```

En effectuant la même requête, cette fois ci avec le Bearer Token, l'accès à l'API est maintenant autorisé :
```
curl https://track.everysens.com/rtls-things-app/api-info \
  -H "Authorization: Bearer $ACCESS_TOKEN"
```
 
Pour le refresh/revoke des tokens et autres actions: tout fonctionne suivant OAuth 2.0.  
N'hésitez pas à vous rapprocher de votre contact technique chez Everysens en cas de problème.